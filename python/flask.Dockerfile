FROM python:3.10.4-alpine
RUN apk add --no-cache gcc musl-dev linux-headers
ADD requirements.txt .
RUN pip install -r requirements.txt

ADD run.py .
ADD src/ src/

ENV IMLADRIS_HOST=0.0.0.0 \
    IMLADRIS_PORT=5000 \
    IMLADRIS_DEBUG=False \
    IMLADRIS_THREADS=4 \
    FLASK_ENV=production

CMD python3 run.py
