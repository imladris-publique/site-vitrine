#! /bin/python3.10
#-*- coding: utf-8 -*-
docs="""
Module de fonctions à insérer dans le contexte de jinja pour les utiliser dans les templates
"""

def map_box(lon,lat,zoom,separator=','):
    """Renvoit en csv les coordonnées des 4 coins d'une box, centrée sur longitude et latitude, à ce niveau de zoom"""
    part = 180.0/(2**zoom)
    box = (lon-part,lat-part,lon+part,lat+part)
    return separator.join(map(str,box)) # map convertit en string box,  separator.join réunis les éléments par separator

utils = dict(map_box=map_box)
