#! /bin/python3.10
#-*- coding: utf-8 -*-

from flask import Blueprint, render_template, request
from flask_assets import Bundle
from .template_utils import utils

# module pour le site vitrine.
#   templates dans vitrine.templates.vitrine
#   fichiers statiques : vitrine.static.{repo} -> /assets/{repo}/
module = Blueprint('vitrine', __name__,
    template_folder='templates',
    static_folder='static', static_url_path='assets'
    )

# sous module pour les fichiers d'images statiques : vitrine.images -> /images/
module_images = Blueprint('vitrine_img', __name__,
    template_folder=None,
    static_folder='images', static_url_path='images'
    )
module.register_blueprint(module_images)

# Bundle de javascript et de css
# L'ensemble des scripts CSS et JS sont compilés dans un seul et même fichier, minimifié.
# Ces fichiers sont automatiquement compilés dans le docker, dans le répertoire /src/vitrine/static/
# Ils sont directement utilisables à l'url /assets/ (selon la config précédente)
# Note : en mode débug 
assets = {
    'vitrine_css' : Bundle('vitrine/css/*.css', filters='cssmin', output='vitrine/vitrine.css'), # compilé dans le docker à /src/vitrine/static/vitrine.css
    'vitrine_js'  : Bundle('vitrine/js/*.js',   filters='jsmin',  output='vitrine/vitrine.js')   # compilé dans le docker à /src/vitrine/static/vitrine.js
}



# Importe les fonctions de templates dans le module
module.context_processor(lambda : utils) # prend en paramètre une fonction, qui ici renvoit uniquement utils

# ----------------------- Routes -------------------- #


module.compteur = 1

@module.route('/test', methods=['GET'])
def test():
    """
    root page view that returns the index page and its data
    :return: index template
    """
    module.compteur += 1
    # season_anime = get_season_anime()
    return render_template('vitrine/home.html', my_list=['toto','titi','tutu', module.compteur])


@module.route('/', methods=['GET'])
@module.route('/index.html', methods=['GET'])
def index():
    """
    Pages d'accueil
    """
    return render_template('vitrine/index.html')

@module.route('/left-sidebar.html', methods=['GET'])
def left_sidebar():
    """
    page left_sidebar
    """
    return render_template('vitrine/left-sidebar.html')

@module.route('/right-sidebar.html', methods=['GET'])
def right_sidebar():
    """
    page right_sidebar
    """
    return render_template('vitrine/right-sidebar.html')

@module.route('/no-sidebar.html', methods=['GET'])
def no_sidebar():
    """
    page right_sidebar
    """
    return render_template('vitrine/no-sidebar.html')

@module.route('/elements.html', methods=['GET'])
def elements():
    """
    page elements
    """
    return render_template('vitrine/elements.html')
