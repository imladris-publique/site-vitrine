#! /bin/python3
#-*- coding: utf-8 -*-

# src/__init__.py

doc = """
Installe le serveur sur le port indiqué en paramètre -p ou --port (defaut 5000)
Ajoute le module Blueprint vitrine
"""


import sys
from pathlib import Path
from flask import Flask
from flask_assets import Environment, Bundle
from .environnement import arguments

# -------------------- Répertoire de plus haut niveau ---------------------- #

# Ajoute le répertoire src comme base de recherche de module
root = Path(__file__).parent.resolve()
if not str(root) in sys.path :
	sys.path.append(str(root))

# -------------------- application ---------------------- #

app = Flask(__name__)
assets = Environment()  # Create an assets environment
assets.init_app(app)  # Initialize Flask-Assets

#TODO app.secret_key = 'dsdfdsf' // Permet à Flask d'utiliser des cookies signé, en particulier pour usage avec les sessions.
# Se renseigner sur l'extension flask_jwt_extended // https://www.datasciencelearner.com/flask-token-based-authentication-secure-api/

## Modules ( Blueprint )
with app.app_context():

	assets.auto_build = True
	assets.debug = arguments.debug

	# Importation des modules
	from vitrine.controlleur import module as module_vitrine, assets as assets_vitrine

	# Affectation des controllers (module blueprint)
	app.register_blueprint(module_vitrine, url_prefix='/')

	# Gestion des assets
	build = app.config['ENV'] == 'development'
	for name, one in assets_vitrine.items() :
		assets.register(name,one)
		if build and len(one.resolve_contents()):
			one.build()
