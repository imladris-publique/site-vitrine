

Méthodes de classes
-------------------

__slots__ = ('a','b','c')

Tuple définissant toutes les propriétés accessibles pour l'objet.
Permet de bloquer la possibilier d'initialiser une propriété d'un objet ne faisant par partit du slot.
Définir un __slots__ dans la classe est très pratique, car il permet de déclancher des exceptions dans le cas de set ou
de get d'un attribut qui ne devrait pas être saisis.
Ne sont pas concernés les méthodes et propriétés définies dans l'objet.


__repr__(self):
Renvoit la représentation envoyé dans la console.



__del__(self) est toujours appelé à la destruction de l'objet. (permet de libérer des ressources)

### Cast
string  : __str__	: utilisé pour print()
int 	: __int__
float	: __float__



class MaClasse
	propriété = property( getter, setter, deller )

Permet de définir les getter, setter et deller utilisé pour la propriété.
A surtout l'énorme intérêt de faire des propriétés en lecture seule si seul le getter est défini.
Note : "propriété" n'a pas à être défini dans le __slot__ puisqu'il s'agit uniquement de méthodes.


__getitem__(self, key)
__setitem__(self, key, value)
Permet de définir le comportement si l'instance est appelée comme un tableau avec instance[key]

__len__(self)
Appelé si on fait len(objet)

__contains__(self,item)
Appelé si on teste ``item in objet``

__iter__(self)
Appelé quand on fait iter(objet) ou dans les boucles for



#TODO :

    # Appelé quand on fait reversed(objet)
    def __reversed__(self):
        return reversed(self.cartes)



### Override des opérateurs :

+	: __add__(self, other)
-	: __sub__(self, other)
* 	: __mul__(self, other)
/	: __div__(self, other)
%	: __mod__(self, other)
**	: __pow__(self, other)
<<	: __lshift__(self, other)
>>	: __rshift__(self, other)
&	: __and__(self, other)
|	: __or__(self, other)
<	: __lt__(self, other)
<=	: __le__(self, other)
==	: __eq__(self, other)
!=	: __ne__(self, other)
>	: __gt__(self, other)
>=	: __ge__(self, other)




__getattr__(self, name)	: lorsqu'on appelle un attribut qui n'existe pas.
__delattr__(self, name) : lorsqu'on del un attribut qui n'existe pas
__setattr__(self, name, value): lorsqu'on définit un attribut qui n'existe pas.




__get__ et __set__ ( et __set_name__ )

Utilisé dans les classes de descripteurs.
Ces classes permettent de décrire une méthode indépendante d'une classe pour obtenir ou définir une propriété.

ex :
```
class Celsius:
	""" Méthode pour convertir une propriété en fahrenheit en celsius """

    def __get__(self, instance, owner):
        return 5 * (instance.fahrenheit - 32) / 9

    def __set__(self, instance, value):
        instance.fahrenheit = 32 + 9 * value / 5


class Temperature:
	"""Notre classe intégrant une propriété en fahrenheit et un descripteur pour la convertion"""
    celsius = Celsius()

    def __init__(self, initial_f):
        self.fahrenheit = initial_f

temp=Temperature()
temp.celsius		# appelle celsius.__get__(descripteur, temp, Temperature)

```

Une même instance de descripteur peut être un objet partagé par toutes les instances. (mode singleton)
Mais là où ça peut devenir puissant (et complexe), c'est que le discripteur peut posséder ou être initialisé avec différentes propriétés.

Le descripteur pourrait permettre de définir une origine (dans un graffe), et les objets sortir leur propriété par rapport à cette origine.
Changer les propriétés de l'origine (de l'objet descripteur) permet de changer immédiatement les coordonnées sortie par tous les objets.

__set_name__(self, owner, name) est introduit en python 3.6
Cette méthode est utilisée lors de la création du descripteur, pour lui passer en paramètre le nom de l'attribut sur lequel il est définit.

Pour l'exemple précédent, l'initialisation de la propriété celsius dans la classe Temperature appellera __set_name__(Celsius, Temperature, 'celsius')
Ce mécanisme permet de définir au sein du descripteur des comportements qui dépendent du nom de la propriété. (par exemple, pour aller chercher le _propriété)



__match_args__

Définie une liste de propriétés utilisée pour les match dans la procédure "match-case"
cf : https://renanmf.com/python-match-case/


exemple :

```
class Point2D:
    """A class to represent points in a 2D space."""

    __match_args__ = ["x", "y"]
    def __init__(self, x, y):
        self.x = x
        self.y = y

def describe_point(point):
    """Write a human-readable description of the point position."""

    match point:
        case Point2D(0, 0):
            desc = "at the origin"
        case Point2D(0, y):
            desc = f"in the vertical axis, at y = {y}"
        case Point2D(x, 0):
            desc = f"in the horizontal axis, at x = {x}"
        case Point2D(x, y):
            desc = f"at {point}"

    return "The point is " + desc

# Prints "The point is at the origin"
print(describe_point(Point2D(0, 0)))
# Prints "The point is in the horizontal axis, at x = 3"
print(describe_point(Point2D(3, 0)))
# Prints "# The point is at (1, 2)"
print(describe_point(Point2D(1, 2)))

```


```( https://benhoyt.com/writings/python-pattern-matching/ )

class Car:
    __match_args__ = ('key', 'name')
    def __init__(self, key, name):
        self.key = key
        self.name = name

expr = eval(input('Expr: '))
match expr:
    case (0, x):              # seq of 2 elems with first 0
        print(f'(0, {x})')    # (new variable x set to second elem)
    case ['a', x, 'c']:       # seq of 3 elems: 'a', anything, 'c'
        print(f"'a', {x!r}, 'c'")
    case {'foo': bar}:        # dict with key 'foo' (may have others)
        print(f"{{'foo': {bar}}}")
    case [1, 2, *rest]:       # seq of: 1, 2, ... other elements
        print(f'[1, 2, *{rest}]')
    case {'x': x, **kw}:      # dict with key 'x' (others go to kw)
        print(f"{{'x': {x}, **{kw}}}")
    case Car(key=key, name='Tesla'):  # Car with name 'Tesla' (any key)
        print(f"Car({key!r}, 'TESLA!')")
    case Car(key, name):      # similar to above, but use __match_args__
        print(f"Car({key!r}, {name!r})")
    case 1 | 'one' | 'I':     # int 1 or str 'one' or 'I'
        print('one')
    case ['a'|'b' as ab, c]:  # seq of 2 elems with first 'a' or 'b'
        print(f'{ab!r}, {c!r}')
    case (x, y) if x == y:    # seq of 2 elems with first equal to second
        print(f'({x}, {y}) with x==y')
    case _:
        print('no match')
```




Accesseurs
----------

```
parameters  = property( get_parameters, set_parameters, del_parameters )
nbParameters= property( get_nbParameters )
info        = property( get_info )

def __repr__(self):return "Model() %s\n[%s]"%(self.get_info(), ' ; '.join(map(str,self._parameters)))
def __str__(self): return self.get_info()


@property
def nom(self):			return self._nom

@nom.setter
def set_nom(self,nom):	self._nom = nom

```




Raccourcis @
------------


### dataclasses

https://docs.python.org/3/library/dataclasses.html#module-dataclasses


#### dataclass
Génère automatiquement une grande série de méthodes magiques.
Fonctionne à partir des propriétés déclarées, leur type et valeur par défaut.
Traite souvent l'objet comme s'il s'agissait d'un tuple des propriétés déclarées. (en particulier pour les méthodes d'égalité ou de comparaison)


```
@dataclass
@dataclass()
@dataclass(init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False, match_args=True, kw_only=False, slots=False)
class Test:
	coucou: int = 0
	truc: str

```

__init__ (True) :
Génère automatiquement le constructeur à partir des informations des propriétés. En particulier avec les valeurs par défauts.

__repr__ (True) :
crée une représentation à partir du nom de la classe et du tuple des propriétés.


__eq__ (True)
**order** (False) -> __lt__, __le__, __gt__, __ge__ :

Permet la comparaison entre les objets.


**unsafe_hash** (False) -> __hash__
Cette méthode est très dépendante de la méthode __eq__. Attention à son utilisation normalement réservée à des objets immutables.
Voir la doc pour plus d'information.

**frozen** (False) -> __setattr__, __delattr__ :
Bloque les propriétés en lecture seule, pour créer des objets immutables.
À utiliser en lien avec la propriété précédente.
Attention, si frozen=True et __init__ déclarée manuellement, on ne peut utiliser une assignation directe (elle est bloquée), il faut utiliser la méthode  object.__setattr__()



__match_args__ (True) :
Génère automatiquement le tuple de __match_args__, même si la méthode __init__ n'est pas généré automatiquement.


**kw_only** (False) :
on ne peut utiliser dans le constructeur que les mots clefs des arguments.
ex :  ` def func(arg, *, kw_only1, kw_only2): ... `
-> les propriétés kw_onlyX ne peuvent être saisie qu'en précisant expressément le nom de l'argument.


__slots__ (False) :



#### field()

Permet de préciser dans les spécifications des attributs de classe la méthode pour les initialiser.

```

@dataclass
class C:
    x: int
    t: int = 20
    y: int = field(repr=False)
    z: int = field(repr=False, default=10)
    mylist: list[int] = field(default_factory=list)
```

- **default** : La valeur par défaut à utiliser.
- **default_factory** : Un constructeur, utilisable sans arguments. Attention, ne pas utiliser avec 'default'
- **init** : (bool:True) -> propriété inclue dans le constructeur. Préciser False pour ne pas l'utiliser.
- **repr**: (bool:True) -> propriété utilisée pour la représentation __repr__
- **hash**: (bool:None) -> True -> utilisée pour la méthode __hash__. None -> même valeur que **compare**, ce qui est la valeur désiré. L'usage autre que None est découragé. Une des raisons possible d'utiliser hash=False et compare=True pourrait être que le hash nécessite un gros travail. Une autre propriété pouvant être utilisée à la place.
- **compare**: (bool:True) -> Utilise la propriété pour la comparaison. (__eq__, __gt__ ...)
- **metadata** (None) : Un mapping ou None. None est traité comme un dictionnaire vide. Cette valeur est poussée dans un MappingProxyType pour la rendre read-only et exposer dans le champs de l'objet. Il n'est pas utilisé par le Data Classes...
- **kw_only**(bool) : True -> propriété marquée comme keyword-only pour la génération de __init__.



__post_init__() : Méthode appelée dans le constructeur généré automatiquement. Sert souvent pour utiliser le constructeur de la classe parent.
Note pour les paramètres obligatoires :
Les attributs déclarée sont transmit à __post_init__. Certain paramètres ne doivent pas donner naissance à une propriété propre à l'objet (définie dans le parent), on doit alors déclarer ces propriétés comme init-only. Elles seront passée au constructeur (et au __post_init__) mais ne donneront pas naissance à une propriété dans la classe actuelle.
Pour déclarer une propriété comme init-only :
```
@dataclass
class C:
    i: int
    j: int = None
    database: InitVar[DatabaseType] = None

    def __post_init__(self, database):
        if self.j is None and database is not None:
            self.j = database.lookup('j')
```
