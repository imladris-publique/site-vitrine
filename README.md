# Site vitrine



## Arborescence du code

- `docker-compose.yml`  
  Le fichier docker-compose pour lancer et configurer votre application sur votre serveur.
- `docker-compose.override.yml` (optionnel, non gitté)
  Permet de surcharger le docker-compose pour effectuer des configurations locales. Ces informations ne sont pas gités
- `site`  
  Répertoire qui permet de surcharger "à la vollée" les templates utilisés par le site. Ce mécanisme permet d'adapter ou même d'ajouter des pages au site, sans avoir à re-compiler le docker file, et sans même avoir à redémarer le docker.
  - `site/vitrine`  
  Répertoire permettant de surcharger les templates du module "vitrine"
- `python/`  
  Répertoire concernant l'application ( codée en python ). ( Note : le répertoire ne peut pas s'appeler "app" ). Ce répertoire contient notemment le dockerfile permettant la création de l'application, le script de lancement du programme (run.py) et les sources de l'application (répertoire src)
  - `flask.Dockerfile`  
    Le fichier de création du docker de votre application.
  - `python/requirements.txt`  
    Définies les modules de python à installer pour que votre code puisse les importer.
  - `python/run.py`
    Le fichier de lancement de l'application. ( ne sert qu'à lancer l'application au démarrage du docker )
  - `python/src/`  
    Contient toutes les sources de l'application.
    - `python/src/__init__.py`  
      Crée l'application flask, définie les différents modules (blueprint) et leur attribues les pérfixes dans l'url, gère et compile les assets (fichier static comme le CSS ou le JS) des différents modules.
    - `python/src/environnement.py`  
      Définie les variables d'environnements utilisées dans le programmes, leur valeur par défaut, le formattage etc...
    - `python/src/templates`  
    Répertoire non utilisé, permettant en théorie de surcharger à la volée les templates des différents modules. Ce répertoire est remplacé par le contenu de /site/ au lancement du docker (grâce aux volumes du docker-compose)
      - `python/src/vitrine/`  
        Module du site vitrine
        - `python/src/vitrine/controlleur.py`  
          Controlleur de l'application, définie les chemins et les fonctions associés qui seront lancée pour ces chemins. Définie également les assets (fichiers statics js ou css) utilisés par le module.
        - `python/src/vitrine/template_utils.py`  
          Ensembles de variables (notamment des fonctions) qui seront intégrées dans le moteur de template, pour pouvoir être utilisée.
        - `python/src/vitrine/images`  
          Répertoire où sont stockés les images. Attention, ce répertoire est non gité.
        - `python/src/vitrine/static`  
          Répertoire contenant les scripts statics (css ou js) envoyé au serveur. En particulier :
          - `python/src/vitrine/static/css` Pour les scripts CSS
          - `python/src/vitrine/static/js` Pour les scripts javascript
          - `python/src/vitrine/static/webfonts` Pour les polices intégrés au site
          - `python/src/vitrine/static/sass` Pour les scripts en sass à compiler en css par le biais des assets ( cf `python/src/vitrine/controlleur.py` )


## Variables d'environnements


### Préfix

Commencer ses variables d'environnement par un préfixe permet d'éviter les erreurs de recoupement (autrement appelés effets de bords)  

Plusieurs programmes, ou module dans un même programme peuvent utiliser des variables d'environnement pour être configurés.
Par inadvertances, il peut arriver qu'on choisise pour son application un nom de variable d'environnement déjà utilisé par un autre de ces programmes ou module.

Faire commencer les variables d'environnement de son application par un préfixe permet d'éviter ces erreurs. De surcroit, ceci permet de "ranger" toutes les variables d'environnement concernant notre application en effectuant une recherche sur toutes celles commençant par ce préfixe.

Dans ce code, toutes nos variables d'environnement "maison" commencent par IMLADRIS_.


## Adaptation à votre site

### Variables d'environnement
Le préfixe des variables d'environnements sont à adapter à chaque site.

Remplacer dans tous les fichiers :
IMLADRIS_ par VOTRE_PREFIXE_

Normalement, les seuls fichiers où sont sensé être définis les variables d'environnements sont :
- docker-compose.yml ou docker-compose.override.yml  
  Qui configure les variables d'environnements pour votre programme.
- python/flask.Dockerfile  
  Le docker file qui définie toutes les variables d'environnements envoyées par défaut dans le conténaire
- python/src/environment.py  
  Le code qui définie (entre autre) quelles variables d'environnements sont utilisées et extraite par le programme

### Le title et l'icone du site
( Le nom de l'onglet dans le navigateur )
Dans le répertoire `/python/src/vitrine/templates/vitrine/includes/head.html`

## Déploiement

### Fichiers non gités
Attention, plusieurs répertoires ne sont pas gités, et nécessitent un transfert de fichier sur le serveur.

- `/secrets/`  
    Contient les secrets docker
- `/python/src/vitrine/images/`  
    Contient les images du site
- `/python/src/vitrine/static/webfonts/`  
    Contient les polices utilisées sur le site

```
rsync -avzc -e "ssh -i $CHEMIN_CLEF_SSH -p $PORT_SSH_SERVEUR"

rsync -azc $SOURCE/python/src/vitrine/images/ $DESTINATION/python/src/vitrine/images/
rsync -azc $SOURCE/python/src/vitrine/static/webfonts/ $DESTINATION/python/src/vitrine/static/webfonts/

# Ne jamais recopier les secrets docker. En créer de nouveaux sur le serveur, adapté à un niveau "prod"

```




### Variables d'environnement

Les secrets docker sont à créer.
Toutes les variables d'environnements utilisées par le site peuvent automatiquement être suffixés par _FILE afin d'être chargée par le biais d'un secret docker.
( pour un aperçu exaustif des variables d'environnements utilisées par l'application : cf `python/src/environnement.py` )

### docker-compose.override.yml
Vous pouvez surcharger le docker-compose en créant un fichier `docker-compose.override.yml`.
Ceci permet d'adapter votre déploiement sur votre serveur, sans modifier le fichier `docker-compose.yml` qui est lui gité.
Ceci simplifiera la mise à jour de votre application en effectuant simplement un git pull des dernières modifications.
Ceci permettra également d'être plus sécurisé car le fichier `docker-compose.override.yml` ne sera jamais gité (donc vos configurations ne seront pas publiés)

Il est notamment intéressant de configurer ses variables d'environnement dans ce fichier. (même les secrets docker, afin de ne pas même giter les chemins où se trouverons ces secrets dans votre conténaire)
C'est aussi dans ce fichier que nous choisissons les réseaux docker sur lesquels serons connectés les dockers de nos applications.
Vous pouvez également utiliser ce fichier pour ouvrir et effectuer les redirections de ports que vous souhaitez utiliser sur votre server pour cette application.


## Liens utiles :

- blueprint :
  - https://realpython.com/flask-blueprint/#how-to-use-flask-blueprints-to-improve-code-reuse
  - https://exploreflask.com/en/latest/blueprints.html
  - https://medium.com/innovation-res/flask-blueprints-tutorial-74862e5a19fd
  - En particulier pour les Bundle d'assets :  
    http://hackersandslackers.io/flask-blueprints/
  - Config sérieuse de Flask :  
    https://flask.palletsprojects.com/en/2.1.x/config/
- assets
  - https://flask-assets.readthedocs.io/en/latest/
  - https://webassets.readthedocs.io/en/latest/bundles.html#bundles
  - https://webassets.readthedocs.io/en/latest/css_compilers.html
- cache
  - https://flask-caching.readthedocs.io/en/latest/index.html

- jinja :
  - https://jinja.palletsprojects.com/en/3.1.x/templates/
  - https://jinja.palletsprojects.com/en/3.0.x/templates/#macros
- BDD (à plat, SQL, sqlalchemy ... )
  - https://www.membershipsthatpay.com/gestion-des-donnees-avec-python-sqlite-et-sqlalchemy-real-python/
- Serveur de prod waitress
  - https://flask.palletsprojects.com/en/2.1.x/tutorial/deploy/
  - https://docs.pylonsproject.org/projects/waitress/en/stable/arguments.html
- python
  - switch - case en python avec match :  
    https://benhoyt.com/writings/python-pattern-matching/
